package com.fobos.test.oauth.oauth_test.controller;


import com.fobos.test.oauth.oauth_test.request.MyIdToken;
import com.google.api.client.auth.openidconnect.IdToken;
import com.google.api.client.extensions.appengine.http.urlfetch.UrlFetchTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken.Payload;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Collections;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping("/google")
public class GoogleController {

    private static final JacksonFactory jacksonFactory = new JacksonFactory();
    private static final UrlFetchTransport urlFetchTransport=new UrlFetchTransport();
    @RequestMapping(path="/id_token", method = POST, consumes = "application/json")
    public void googleOauthCallback(@RequestBody MyIdToken token) throws GeneralSecurityException, IOException {
         GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(new NetHttpTransport(), jacksonFactory)
                 .setAudience(Collections.singletonList("211080256159-r0umedmdf2mph44n38mm0jrv63v07oo9.apps.googleusercontent.com"))
                 .build();
         GoogleIdToken idToken = verifier.verify(token.getToken());
         if (idToken != null) {
             Payload payload = idToken.getPayload();

             // Print user identifier
             String userId = payload.getSubject();
             System.out.println("User ID: " + userId);
             // Get profile information from payload
             String email = payload.getEmail();
             boolean emailVerified = payload.getEmailVerified();
             String name = (String) payload.get("name");
             String pictureUrl = (String) payload.get("picture");
             String locale = (String) payload.get("locale");
             String familyName = (String) payload.get("family_name");
             String givenName = (String) payload.get("given_name");

             // Use or store profile information
             // ...

         } else {
             System.out.println("Invalid ID token.");
         }

    }

}
