package com.fobos.test.oauth.oauth_test.controller;


import com.fobos.test.oauth.oauth_test.request.FirebaseToken;
import facebook4j.*;
import facebook4j.conf.ConfigurationBuilder;
import facebook4j.internal.http.HttpParameter;
import facebook4j.internal.org.json.JSONArray;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping("/facebook")
public class FacebookController {


    @RequestMapping(path="/id_token", method = POST, consumes = "application/json")
    public void twitterCallback(@RequestBody FirebaseToken token) throws FacebookException {
        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setDebugEnabled(true)
                .setOAuthAppId("1687494584654193")
                .setOAuthAppSecret("4ccc09fd1c466eb6095dd7ca901e5ccd")
                .setOAuthAccessToken(token.getAccessToken())
                .setOAuthPermissions("email,public_profile,user_friends,manage_pages,pages_show_list")
                ;
        FacebookFactory ff = new FacebookFactory(cb.build());
        Facebook facebook = ff.getInstance();
        User user =  facebook.getMe();
        String fields="about,id,age_range,birthday,context,cover,currency,devices,education,email,favorite_athletes,favorite_teams,first_name,gender,hometown,inspirational_people,install_type,installed,interested_in,is_shared_login,is_verified,languages,last_name,link,locale,location,meeting_for,middle_name,name,name_format,payment_pricepoints,political,public_key,quotes,relationship_status,religion,security_settings,shared_login_upgrade_required_by,significant_other,sports,test_group,third_party_id,timezone,updated_time,verified,video_upload_limits,viewer_can_send_gift,website,work";
        User user2 =  facebook.getMe(new Reading().fields(fields));
        user2.getPicture();
    }

}
