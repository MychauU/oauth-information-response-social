package com.fobos.test.oauth.oauth_test.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fobos.test.oauth.oauth_test.response.OauthResponse;
import com.google.api.client.json.JsonParser;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.gson.Gson;
import org.json.JSONObject;
import jdk.nashorn.internal.parser.JSONParser;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
@RequestMapping("/paypal")
public class PayPalController {

    private String clientId="ASTAUK42N2nwqy2ZJuglCfzCyUvhga-9a1hOuDKdUTL6KCgRdB8ClMCvy_NjXPMNA8d0WEB-mWd-j3LC";
    private String secret="ENO-qfFbnOlfGZJWJzX1Xc4ZP0DLGwS6XVyWPCq1LxZ0T6uVt02OT1INSshlo8QlIQ2ys8J2J8tQpCxg";

    private String clientIdProd="AXfAOncCF8U74lJNfT7PMZgL50NGyDtssd06KyYsJm2XGDA453h2hAg0qcUsqodOLvBmT9xOAx2gWfUL";
    private String secretProd="EMFRtiKZjKiEakrouL9zJEFuQGfgGPODWmCpBeTHKls17otWete-xYoyXOYLEhEpNESwYqkcH0mTjQZc";

    @RequestMapping(method = GET, path="/callback")
    public ResponseEntity<Void> paypalCallback(@RequestParam("scope") String scope, @RequestParam("code") String code ) throws IOException {
        HttpHeaders headers = new HttpHeaders();
     //   headers.add("Expires","Thu, 21 Sep 2017 11:07:49 GMT");
        headers.add("Accept","application/json");
        headers.add("Accept-Language","en_US");
        RestTemplate restTemplate =new RestTemplate();
        MultiValueMap<String, String> body = new LinkedMultiValueMap<String, String>();
        body.add("client_id",clientId);
        body.add("client_secret",secret);
        body.add("grant_type","authorization_code");
        body.add("code",code);
        //body.add("return_url","http:127.0.0.1:8081/");
        HttpEntity<?> entity = new HttpEntity<Object>(body, headers);
        String asd=entity.toString();
        ResponseEntity<Object> response= restTemplate.exchange("https://api.sandbox.paypal.com/v1/identity/openidconnect/tokenservice", HttpMethod.POST,entity,Object.class);
        ObjectMapper mapper = new ObjectMapper();
        String responseString=mapper.writeValueAsString(response.getBody());
        JSONObject jsonObject=new JSONObject(responseString);
        String accessToken=(String)jsonObject.get("access_token");
        restTemplate =new RestTemplate();
        headers = new HttpHeaders();
        headers.add("Accept","application/json");
        headers.add("Authorization", "Bearer "+accessToken);
        entity = new HttpEntity<Object>(headers);
        response= restTemplate.exchange("https://api.sandbox.paypal.com/v1/oauth2/token/userinfo?schema=openid", HttpMethod.GET,entity,Object.class);
        return ResponseEntity.ok().build();
    }

    @RequestMapping(method = GET, path="/callback-prod")
    public ResponseEntity<Void> paypalCallbackProd(@RequestParam("scope") String scope, @RequestParam("code") String code ) throws IOException {
        HttpHeaders headers = new HttpHeaders();
        //   headers.add("Expires","Thu, 21 Sep 2017 11:07:49 GMT");
        headers.add("Accept","application/json");
        headers.add("Accept-Language","en_US");
        RestTemplate restTemplate =new RestTemplate();
        MultiValueMap<String, String> body = new LinkedMultiValueMap<String, String>();
        body.add("client_id",clientIdProd);
        body.add("client_secret",secretProd);
        body.add("grant_type","authorization_code");
        body.add("code",code);
        //body.add("return_url","http:127.0.0.1:8081/");
        HttpEntity<?> entity = new HttpEntity<Object>(body, headers);
        String asd=entity.toString();
        ResponseEntity<Object> response= restTemplate.exchange("https://api.paypal.com/v1/identity/openidconnect/tokenservice", HttpMethod.POST,entity,Object.class);
        ObjectMapper mapper = new ObjectMapper();
        String responseString=mapper.writeValueAsString(response.getBody());
        JSONObject jsonObject=new JSONObject(responseString);
        String accessToken=(String)jsonObject.get("access_token");
        restTemplate =new RestTemplate();
        headers = new HttpHeaders();
        headers.add("Accept","application/json");
        headers.add("Authorization", "Bearer "+accessToken);
        entity = new HttpEntity<Object>(headers);
        response= restTemplate.exchange("https://api.paypal.com/v1/oauth2/token/userinfo?schema=openid", HttpMethod.GET,entity,Object.class);
        return ResponseEntity.ok().build();
    }

    @RequestMapping(path="/agreement", method = GET, consumes = "application/json")
    public ResponseEntity<String> paypalAgreement()   {
        return ResponseEntity.ok().body("agreement");
    }

    @RequestMapping(path="/policy", method = GET, consumes = "application/json")
    public ResponseEntity<String> paypalPolicy()   {
        return ResponseEntity.ok().body("policy");
    }
}
